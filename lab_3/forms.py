from django.db.models import fields
from django.http import request
from lab_1.models import Friend
from django import forms

class FriendForm(forms.ModelForm) :
    class Meta :
        model = Friend
        fields = ['name', 'NPM', 'birth_date']

    error_messages = {
        'required' : 'Tolong isi field yang belum diisi >w<'
    }
    
    name = forms.CharField(label = 'Nama', required = True, 
    max_length = 30, widget = forms.TextInput(attrs = {'type' : 'text', 'placeholder' : 'Nama lengkap kamu'}))

    NPM = forms.CharField(label = "NPM", required = True,
    max_length = 30, widget = forms.TextInput(attrs = {'type' : 'text', 'placeholder' : 'NPM kamu'}))

    birth_date = forms.DateField(label = "Tanggal Lahir", required= True,
    widget= forms.DateInput(attrs={'type' : 'date', 'placeholder' : 'Tanggal lahir kamu'}))


