from django.urls.conf import path

from lab_5.views import index

urlpatterns = [

    path('', index, name = "index-lab5"),

]