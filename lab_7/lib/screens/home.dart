import 'package:flutter/material.dart';
import 'package:lab_7/widgets/card.dart';
import 'package:lab_7/widgets/cardMeninggal.dart';
import 'package:lab_7/widgets/cardSembuh.dart';
import 'package:lab_7/widgets/carousel.dart';
import 'package:lab_7/widgets/krisanForm.dart';

class HomeIsYou extends StatefulWidget {
  const HomeIsYou({Key? key}) : super(key: key);

  @override
  _HomeIsYouState createState() => _HomeIsYouState();
}

class _HomeIsYouState extends State<HomeIsYou> {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      DataCovidCarousel(),
      CardCovidPositf(),
      CardCovidSembuh(),
      CardCovidMeninggal(),
      KrisanForm(),
    ]);
  }
}
