import 'package:flutter/material.dart';
import 'package:lab_6/main.dart';

class DataCovid extends StatefulWidget {
  const DataCovid({Key? key}) : super(key: key);

  @override
  State<DataCovid> createState() => _DataCovid();
}

class _DataCovid extends State<DataCovid> {
  // List<Journal> dummyJournal = [];

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          // ignore: sized_box_for_whitespace
          Container(
            height: 64,
            child: const DrawerHeader(
              child: Text(
                'Invid19',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
              ),
              decoration: BoxDecoration(
                color: Color(0xFF24262A),
              ),
            ),
          ),
          ListTile(
            title: const Text('Home'),
            onTap: () {
              // Go to Riwayat Jurnal screen
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Berita'),
            onTap: () {
              // Go to Jurnal Baru page
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Artikel'),
            onTap: () {
              // Go to Jurnal Baru page
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Vaksinasi'),
            onTap: () {
              // Go to Jurnal Baru page
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Profile'),
            onTap: () {
              // Go to Jurnal Baru page
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Forum Diskusi'),
            onTap: () {
              // Go to Jurnal Baru page
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Buat Artikel'),
            onTap: () {
              // Go to Jurnal Baru page
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
