from lab_2.views import index, json, xml
from django.urls.conf import path


urlpatterns = [
    path('', index, name = "index"),
    path('xml', xml, name = "xml"),
    path('json', json, name = "json"),
]



