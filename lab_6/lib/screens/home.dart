import 'package:flutter/material.dart';
import 'package:lab_6/widgets/card.dart';
import 'package:lab_6/widgets/cardMeninggal.dart';
import 'package:lab_6/widgets/cardSembuh.dart';
import 'package:lab_6/widgets/carousel.dart';

class HomeIsYou extends StatefulWidget {
  const HomeIsYou({Key? key}) : super(key: key);

  @override
  _HomeIsYouState createState() => _HomeIsYouState();
}

class _HomeIsYouState extends State<HomeIsYou> {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      DataCovidCarousel(),
      CardCovidPositf(),
      CardCovidSembuh(),
      CardCovidMeninggal(),
    ]);
  }
}
