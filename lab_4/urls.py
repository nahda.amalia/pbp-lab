from django.urls.conf import path

from lab_4.views import add_note, index, note_list

urlpatterns = [

    path('', index, name = "index-lab4"),
    path('add-note', add_note, name= "add-note"),
    path('note-list', note_list, name="note-list"),

]