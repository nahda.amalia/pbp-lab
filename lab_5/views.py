from django.shortcuts import render
from lab_2.models import Note
# from lab_5.forms import NoteForm

# Create your views here.
def index(request) :
    data = Note.objects.all()
    response = {'data' : data}
    return render(request, 'lab5_index.html', response)