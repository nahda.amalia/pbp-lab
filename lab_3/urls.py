from django.urls.conf import path

from lab_3.views import add_friend, index


urlpatterns = [

    path('', index, name = "index"),
    path('add', add_friend, name="teman_teman")
]