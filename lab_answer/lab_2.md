JSON merupakan singkatan dari _JavaScript Object Notation_ yang digunakan untuk menyimpan dan mentransfer data.
XML merupakan singkatan dari _eXtensible Markup Language_ yang digunakan untuk menyimpan dan melakukan perturkaran data.
Jika dilihat dari kegunaannya, fungsi dari JSON dan XML adalah sama. Namun, ada yang membedakannya berdasaarkan kelebihan dan kekurangannya.

Berikut kelebihan dan kekurangan JSON :

| Kelebihan                                     | Kekurangan                                        |
| --------------------------------------------- | ------------------------------------------------- |
| - Mendukung semua browser                     | - Tidak mendukung namespace                       |
| - Mudah dipahami                              | - Tool pengembangan terbatas                      |
| - Sintaksnya yang mudah, bentuknya mirip      | - Tidak memiliki kemampuan tampilan               |
| seperti object JavaScript                     | - Kurang terjamin jika dibandingkan dengan XML    |
| - Sebagian besar teknologi backend mendukung  | - Tidak mendukung comment                         |
| - Hasil dari JSON sangat mudah untuk dibaca   |                                                   |

Berikut kelebihan dan kekurangan XML :

| Kelebihan                                     | Kekurangan                                        |
| --------------------------------------------- | ------------------------------------------------- |
| - Pertukaran data dilakukan dengan cepat      | - Membutuhkan aplikasi pemrosesan                 |
| - XML memisahkan data dari HTML               | - Syntax kadang membingungkan                     |
| - XML menyederhanakan proses perubahan        | - Syntax kadang berlebihan                        |
| platform                                      | - Tidak di-_support_ penuh oleh Ajax              |
| - Sebagian besar teknologi backend mendukung  | - Tidak mendukung comment                         |
| - Hasil dari JSON sangat mudah untuk dibaca   |                                                   |

Berdasarkan kelebihan dan kekurangan yang dipaparkan, XML cocok ketika terdapat sebuah project yang kebutuhan penyimpanan data yang besar namun tetap readable dan maintanable. Sedangkan JSON digunakan ketika kita memiliki pertukaran data yang sederhana.

HTML merupakan singkatan dari _HyperText Markup Language_ yang digunakan untuk membuat sebuah halam web dan menampilkan berbagai informasi di dalam sebuah browser internet. Perbedaan yang dimiliki oleh HTML dan XML adalah :

| HTML                                          | XML                                               |
| --------------------------------------------- | ------------------------------------------------- |
| - Tidak peka terhadap besar kecilnya huruf    | - Peka terhadap besar kecilnya huruf              |
| - Memiliki tag yang telah ditentukan          | - Programmer mendefinisikan kumpulan tagnya XML   |
| sebelumnya                                    | - Tidak memiliki kemampuan tampilan               |
| - Tidak ada tag penutup                       | - Wajib ada tag penutup                           |
| - Berfokus pada menampilkan data              | - Berfokus pada membawa informasi                 |
| - Membantu mengembangkan struktur halaman web | - Membantu untuk bertukar data antara platform    |

Setelah melihat penjabaran perbedaan antara HTML dan XML, HTML digunakan untuk membantu mengembangkan struktur halam web, sementara XML bertujuan untuk membantu bertukar data antar platform yang berbeda.

Referensi :
https://perbedaan.budisma.net/perbedaan-html-dan-xml.html
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.guru99.com/json-vs-xml-difference.html
